package com.stepDefination;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllTestCases {
	
	@Given("^I am in Home page$")
	public void i_am_in_Home_page() throws Throwable {
	   }
	@When("^I search for a valid data$")
	public void i_search_for_a_valid_data() throws Throwable {
	   }
	@Then("^I should see relevant search results$")
	public void i_should_see_relevant_search_results() throws Throwable {
	   }
	@When("^I search for invalid data$")
	public void i_search_for_invalid_data() throws Throwable {
	}
	@Then("^I should see no search results page found$")
	public void i_should_see_no_search_results_page_found() throws Throwable {
	  }
	
	@Given("^I am in home page$")
	public void i_am_in_home_page() throws Throwable {
	    }
	@When("^I click on login link$")
	public void i_click_on_login_link() throws Throwable {
	 }
	@When("^I login with valid credentials$")
	public void i_login_with_valid_credentials() throws Throwable {
	}
	@Then("^I should be able to see my account page$")
	public void i_should_be_able_to_see_my_account_page() throws Throwable {
	}
}