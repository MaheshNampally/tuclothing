Feature: Login
Scenario: Verify login

Given I am in home page
When I click on login link
And I login with valid credentials
Then I should be able to see my account page