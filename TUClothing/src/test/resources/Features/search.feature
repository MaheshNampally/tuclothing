Feature: Search

Scenario: Verify search with valid data

Given I am in home page
When I search for valid data
Then I should see relevant search results

Scenario: Verify search with invalid data

Given I am in home page
When I search for invalid data
Then I should see no search results page found
