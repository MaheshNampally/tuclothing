package search_Functionality;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Search_With_Product_Code {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\hydma\\Documents\\AutomationFolder\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("137122050");
		driver.findElement(By.className("searchButton")).click();
		}
	}	