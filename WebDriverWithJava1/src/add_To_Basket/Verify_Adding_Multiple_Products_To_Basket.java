package add_To_Basket;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Verify_Adding_Multiple_Products_To_Basket {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\hydma\\Documents\\AutomationFolder\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		driver.manage().window().maximize();
			//Adding Product 1
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("Jeans");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		driver.findElement(By.cssSelector(".button.searchButton")).click();
		driver.findElement(By.linkText("Light Wash Denim Skinny Jeans")).click();
		Select sizeDropDown=new Select(driver.findElement(By.cssSelector("#select-size")));
		sizeDropDown.selectByValue("136455864");
		driver.findElement(By.cssSelector("#AddToCart")).click();
		//Adding Product 2
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("Jeans");
		driver.findElement(By.cssSelector(".button.searchButton")).click();
		driver.findElement(By.linkText("Midwash Denim Slim Fit Jeans")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    Select sizeDropDown1=new Select(driver.findElement(By.cssSelector("select[name='select-size']")));
		sizeDropDown1.selectByValue("136502173");
		driver.findElement(By.cssSelector("#AddToCart")).click();
		//Adding Product 3
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("Jeans");
		driver.findElement(By.cssSelector(".button.searchButton")).click();
		driver.findElement(By.linkText("Midwash Super Skinny Jeans With Stretch")).click();
		driver.findElement(By.cssSelector("#select-size")).click();
		driver.findElement(By.cssSelector("option[value='137122130']")).click();
		driver.findElement(By.cssSelector("#AddToCart")).click();
  }
}
