package add_To_Basket;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Add_To_Basket_With_Multiple_Quantity {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\hydma\\Documents\\AutomationFolder\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		driver.manage().window().maximize();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("Jeans");
		driver.findElement(By.className("searchButton")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.linkText("Burgundy Wash Skinny Jeans")).click();
		//Select sortDropDown=new Select(driver.findElement(By.cssSelector("#sortOptions1")));
		//sortDropDown.selectByIndex(0);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.cssSelector("div[data-value='136451265']")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Select quantityDropDown=new Select(driver.findElement(By.cssSelector("#productVariantQty")));
		quantityDropDown.selectByValue("2");
		driver.findElement(By.id("AddToCart")).click();
		driver.close();
		driver.close();
	}

}
