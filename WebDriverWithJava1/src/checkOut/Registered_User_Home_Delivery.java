package checkOut;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Registered_User_Home_Delivery {

public static void main(String[] args) {

		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\hydma\\Documents\\AutomationFolder\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("https://tuclothing.sainsburys.co.uk/");
		
		driver.manage().window().maximize();
		
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("Jeans");
		driver.findElement(By.className("searchButton")).click();
		
		driver.findElement(By.linkText("Burgundy Wash Skinny Jeans")).click();
		driver.findElement(By.cssSelector("div[data-value='136451265']")).click();
		driver.findElement(By.id("AddToCart")).click();
		driver.findElement(By.xpath("//a[@data-testid='checkoutButton']")).click();
		driver.findElement(By.xpath("//a[@data-testid='basketButtonTop']")).click();
		driver.findElement(By.cssSelector("input[id='guest_email']")).sendKeys("hydmahesh@hotmail.com");
		driver.findElement(By.cssSelector("div[class='submit-container']")).click();
		driver.findElement(By.cssSelector("label[for='HOME_DELIVERY']")).click();
		driver.findElement(By.cssSelector("input[data-testid='continue']")).click();
		
		Select titleDropDown=new Select(driver.findElement(By.cssSelector(".ln-c-select.ln-u-push-bottom")));
		titleDropDown.selectByValue("mr");
		
		driver.findElement(By.cssSelector("input[id='address.firstName']")).sendKeys("Mahesh");
		driver.findElement(By.cssSelector("input[id='address.surname']")).sendKeys("Kumar");
		driver.findElement(By.cssSelector("input[id='addressDeliveryhouseNameOrNumber']")).sendKeys("16");
		driver.findElement(By.cssSelector("input[id='addressPostcode']")).sendKeys("UB32FT");
		driver.findElement(By.xpath("//div[@class='.ln-c-button.ln-u-push-bottom']")).click();
		driver.findElement(By.cssSelector("div[class='ln-u-text-align-center']")).click();
}
}
