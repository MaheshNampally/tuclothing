package checkOut;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Registered_User_Click_And_Collect {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\hydma\\Documents\\AutomationFolder\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("https://tuclothing.sainsburys.co.uk/");
		
		driver.manage().window().maximize();
		
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("Jeans");
		driver.findElement(By.className("searchButton")).click();
		
		driver.findElement(By.linkText("Burgundy Wash Skinny Jeans")).click();
		driver.findElement(By.cssSelector("div[data-value='136451265']")).click();
		driver.findElement(By.id("AddToCart")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//a[@data-testid='checkoutButton']")).click();
		driver.findElement(By.xpath("//a[@data-testid='basketButtonTop']")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.cssSelector("input[id='guest_email']")).sendKeys("hydmahesh@hotmail.com");
		driver.findElement(By.cssSelector("div[class='submit-container']")).click();
		driver.findElement(By.cssSelector("label[for='CLICK_AND_COLLECT']")).click();
		driver.findElement(By.cssSelector("div[class='ln-u-text-align-center']")).click();
}
}